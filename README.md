# Bachelor Thesis



[TOC]

## Prerequisites

- Linux PC with 64 Bit Processor and Hardware Acceleration enabled in BIOS
- [Docker](https://docs.docker.com/v17.12/install/) installed
- [Docker-Compose](https://docs.docker.com/compose/install/) installed
- python 3.6+
- Downloaded [.apk Data](http://nextcloud.doodsafe.de/s/EDHMQeLBjpKaBED) and placed it into `/controller/apk`



## Create virtual environment

If you dont want to install the dependencies of this project globally, then use python-venv, which creates a virtual environment where you can run python code from. 

```bash
$ cd /path/to/the/project/environment
$ python3 -m venv my-venv-name
```

Start the environment:

```bash
$ source my-venv-name/bin/activate
```

Install all dependencies:

```bash
$ pip install -r requirements.txt
```



## Start Framework 

Start Framework by:

```bash
$ python3 connector.py
```



## Configure

### MitM Zertifikat in System Zertifikats Speicher installieren

**Wichtig!** Der Emulator im Docker Container muss mit der flag:

```bash
-e EMULATOR_ARGS="-writable-system"
```

ausgeführt weden, damit ein Zertifikat installiert werden kann.

Es wird sich zum emulator Device connected und der *.cacerts/* Ordner auf die Host-Maschine kopiert. Um ein Zertifikat in diesen Ordner hinzu zu fügen, wird das linux tool *keytool* benutzt. Anschließend wird das Directory wieder zurück auf den Emulator gespielt. 

```bash
adb connect 172.17.0.3:5555
# show all devices availible
adb devices
# become root
adb -s emulator-5554 root
# use ip if emulator is not availible
adb -s 172.17.0.3:5555 root 
# remount 
adb remount

## add your cert into the cert folder of android
# pull cacerts from emulator device
adb -s emulator-5554 pull /system/etc/security/ ./cacerts/
# 1. get the certificate ca-cert.pem file and install openssl if not present
# 2. get a hash, which is later the file name.
openssl x509 -inform PEM -subject_hash_old -in YOUR-CERTIFICATE.pem | head -1
# 3. this will produce something like 69241f1f.
# 4. Rename YOUR-CERTIFICATE.pem to 69241f1f.0 so it has the same form as the other certificates in the cacerts directory.
# 5. copy your certificate to cacerts directory
# 6. push back again
adb -s emulator-5554 push cacerts/ /system/etc/security/
# use ip if emulator is not availible
adb -s 172.17.0.3:5555 push cacerts/ /system/etc/security/
```
