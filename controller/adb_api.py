from logger import Log

from adb.client import Client as AdbClient
from adb import InstallError
from dateutil.parser import parse
from util import retry
import time
import datetime
import os

class Adb:
    def __init__(self, host="127.0.0.1", port=5037):
        self.client = AdbClient(host, port)

    @retry(retry_times=5, sleep_time=3)
    def connect(self, port, ip="0.0.0.0"):
        serial = "{}:{}".format(ip, port)
        cmd = "adb connect {}".format(serial)
        correct_result = "connected to {}".format(serial)
        result = os.popen(cmd).read().strip()
        if correct_result in result:
            Log.info("Connected with: {}".format(serial))
            return serial
        else:
            raise ConnectionError("cannot connect to: {}".format(serial))

    def android_emulators(self):
        devices = self.client.devices()
        if not devices:
            Log.error("No connected devices")
        # print(devices[0].serial)
        return devices

    def start_app(self, device, apk):
        try:
            device.shell(
                "monkey -p {} -c android.intent.category.LAUNCHER 1".format(apk.id))
            Log.info("App <{}> is now running!".format(apk.id))
        except Exception as err:
            Log.error(err)

    def device(self, serial):
        try:
            return self.client.device(serial)
        except Exception as err:
            Log.error("Error in connecting to device with serial <{}>".format(serial))
            return None
        

    @retry(retry_times=5, sleep_time=5)
    def install_app(self, device, apk):
        Log.info("installing on device: {}".format(device))
        try:
            if not device.is_installed(apk):
                device.install(apk.path)
                Log.info("App <{}> succesfully installed!".format(apk.name))
                return True
            else:
                Log.warning("App <{}> already installed".format(apk.name))
                return True
        except InstallError as err:
            Log.error("Error installing <{}> on device <{}>".format(apk.path, device))
            return False
            # other exceptions are caught from the retry decorator.

    def is_installed(self, device, apk):
        try:
            return device.is_installed(apk.id)
        except Exception as err:
            Log.error(err)
            return None