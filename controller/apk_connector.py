import abc
#import sys
#sys.path.append("..")  # Adds higher directory to python modules path.

from os.path import isfile, join, dirname, abspath
from os import listdir, rename, chdir, getcwd
from queue import Queue, Empty, Full
from injector import singleton

from aapt.aapt import get_apk_info
from logger import Log
from model import Apk


class DataConnector(object, metaclass=abc.ABCMeta):
    """
    A class to derive from. It is like an interface in Java.
    The derivation from this class ensures that the user of this
    class needs to implement the functions below to be a valid
    connector.
    """
    # make it a thread and register it somewhere
    @abc.abstractmethod
    def get_apk(self):
        raise NotImplementedError(
            'users must define <get_apk> to use this base class')

    @abc.abstractmethod
    def get_apks(self, amount):
        raise NotImplementedError(
            'users must define <get_apks> to use this base class')


class FileSystemConnector(DataConnector):
    """
    This class connects to a given path in the file system, to read APKs
    from it.
    """

    _APK_PATH = "apk"  # should read from env variable

    def __init__(self):
        path = self._get_dir_path(FileSystemConnector._APK_PATH)
        self._apks = self._read_apk_from_dir(path)

    def _get_dir_path(self, dir_extension=None):
        this_file_path = dirname(abspath(__file__))
        if dir_extension:
            return join(this_file_path, dir_extension)
        else: 
            return this_file_path

    def create_apk_file_name(self, file_name): 
        this_file_path = dirname(abspath(__file__))
        apk_path = join(this_file_path, 'apk', file_name)
        apk_info = get_apk_info(apk_path)
        new_apk = Apk(apk_info)
        Log.info("apk file <{}> created.".format(new_apk.id))
        return new_apk

    def _read_apk_from_dir(self, apk_store_path):
        """
        Reads a directory, and yields every file in it.
        """
        for filename in listdir(apk_store_path):
            path = join(apk_store_path, filename)
            if isfile(path):
                yield filename
            else:
                Log.warning("<{}> is no file => skipping!".format(path))

    def get_apk(self):
        apk = None
        try:
            file_name = next(self._apks)
            if file_name:
                apk = self.create_apk_file_name(file_name)
        except StopIteration as done:
            Log.debug("Generator <get_apk> has reached the end.")
        except Exception as general_err:
            Log.error(general_err)
        finally:
            return apk

    def get_apks(self, amount=10):
        apks = []
        try:
            for _ in range(amount):
                apks.append(next(self._apks))
        except StopIteration as done:
            Log.debug("Generator <get_apks> has reached the end.")
        finally:
            return apks


# class MySQLConnector(DataConnector):
#     def get_apk(self):
#         pass

#     def get_apks(self, amount=15):
#         pass

#     def move_to_used(self, apk_name):
#         pass

# class ApkManager(Observer):

#     @singleton
#     def __init__(self):
#         super().__init__()
#         self._file_system_connector = FileSystemConnector()
#         self._database_connector = MySQLConnector()
#         self._queue = ApkQueue()
#         self._queue.attach(self)

#     def _fill_queue(self):
#         while not self._queue.full():
#             apk = self._file_system_connector.get_apk()
#             if not apk:
#                 break
#             else:
#                 self._queue.put(apk)
#                 #self._file_system_connector.move_to_used(apk)

#     def update(self, state):
#         Log.debug("State in update is: {}".format(state))
#         if state == QueueState.EMPTY:
#             self._fill_queue()
#         elif state == QueueState.FULL:
#             # create a new queue here.
#             pass

#     def get_apk(self):
#         return self._queue.get()