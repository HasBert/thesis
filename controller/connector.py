from logger import Log
from docker_api import Docker
from adb_api import Adb
from apk_connector import FileSystemConnector
from elastic_api import ElasticConnector
from util import retry

import asyncio
import threading
import time

loop = asyncio.get_event_loop()
WAIT_TIME = 60


class Controller:
    def __init__(self):
        self._adb = Adb()
        self._docker = Docker()
        elastic_container = self._docker.get_elastic_container()
        self._elastic = ElasticConnector(
            elastic_container.ip, elastic_container.api_port)
        self._connector = FileSystemConnector()
        self._threads = []

    def _work(self, container):
        try:
            Log.info("Waiting {} seconds. Spying device...".format(WAIT_TIME))
            time.sleep(WAIT_TIME)
            self._elastic.manipulate_index("android", container)
        except Exception as error:
            Log.error("ERROR during elastic process.\n {}".format(error))
        finally:
            self._docker.delete_container(container.id)
            self._clear_thread(threading.current_thread())

    @retry(retry_times=5, sleep_time=3)
    def _create_free_container(self):
        free = self._docker.find_free_containers()
        container = free[0] if free else self._docker.scale_up_container()[0]
        try:
            loop.run_until_complete(container.health_check())
        except SystemError as error:
            self._docker.delete_container(container.id)
            # we raise again to trigger the retry function.
            raise 
        return container

    def _create_thread(self, worker, container):
        try:
            kwargs = {"container": container}
            thread = threading.Thread(target=worker, kwargs=kwargs)
            self._threads.append(thread)
            thread.start()
            #self._thread_counter += 1
        except Exception as err:
            Log.error("Error during thread creation!")

    def _clear_thread(self, thread):
        index = self._threads.index(thread)
        popped_thread = self._threads.pop(index)
        Log.info("Deleted <{}>".format(popped_thread.name))

    def _connect_to_device(self, container):
        port = container.adb_port
        serial = self._adb.connect(port)
        # serial = "0.0.0.0:{}".format(port)
        if serial:
            return self._adb.device(serial)

    def _install_app(self, device, apk):
        try:
            return self._adb.install_app(device, apk)
        except Exception as exception:
            Log.error("Error installing App <{}> on device <{}>".format(
                apk.path, device))
            return False

    def _start_app(self, device, apk):
        self._adb.start_app(device, apk)

    def run(self):
        self._elastic.is_available()
        container = self._create_free_container()
        apk = self._connector.get_apk()
        if not apk:
            Log.info("There are no apks left. FINISHED!")
            exit(0)  # kill program
        device = self._connect_to_device(container)
        installed = self._install_app(device, apk)
        if installed:
            self._start_app(device, apk)
            container.apk = apk  # apply apk to container after started
            self._create_thread(self._work, container)
        # else:
        #     self._docker.delete_container(container.id)

    def start(self):
        concurrent = 3
        while True:
            if len(self._threads) <= concurrent:
                free_slots = concurrent - len(self._threads)
                Log.debug("Starting procedur! Free slots: {}".format(free_slots))
                self.run()
            else:
                Log.debug("Waiting for free slots, to install and start an App.")
            time.sleep(1)


Controller().start()
