import docker
import asyncio
import os

from model import AndroidContainer, ElasticContainer
from logger import Log

class Docker:
    def __init__(self):
        self._client = docker.from_env()
        self._compose = Compose("../docker")
        self._containers = []
        self._init_containers()

    @property
    def containers(self):
        return self._containers

    def _init_containers(self):
        if not self._containers:
            containers = self._android_containers()
            if not containers:
                self._compose.start_init()
                containers = self._android_containers()
            self._containers = containers
                

    def _android_containers(self):
        return [AndroidContainer(container)
                for container in self._client.containers.list()
                if AndroidContainer.container_name in container.attrs['Config']['Labels']["com.docker.compose.service"]]

    def get_elastic_container(self):
        return [ElasticContainer(container)
                for container in self._client.containers.list()
                if ElasticContainer.container_name in container.attrs['Config']['Labels']["com.docker.compose.service"]][0]

    def _get(self, container_id):
        containers = [container
                      for container in self._containers
                      if container_id == container.id]
        return containers[0] if containers else None

    def find_free_containers(self):
        return [container
                for container in self._containers
                if not container.apk]

    def scale_up_container(self, scale=1):
        before = self._android_containers()
        self._compose.scale_up_by(len(before), scale)
        after = self._android_containers()
        # we calculate the last container for ourselfs, becuase we can't catch it with the
        # os command which is fired in Compose.scale_up_by
        added_containers = [info
                            for info in after
                            if info not in before]
        self._containers = [*self.containers, *added_containers]
        return added_containers

    def delete_container(self, container_id):
        try:
            Log.info("Deleting container {}".format(container_id))
            container = self._get(container_id).container
            container.stop()
            container.remove(v=True)
            # self.containers.prune()
            Log.info("Container {} deleted!".format(container_id))
            self._containers.remove(container)
        except Exception as err:
            Log.error(err)
        return container_id


class Compose:
    def __init__(self, compose_path):
        # should be placed in env variables
        self.compose_path = "../docker"
        self.container_name = "android-client"

    def scale_up_by(self, amount_containers, scale):
        cmd = "cd {} && docker-compose -f docker-compose.yml -f docker-compose_emulator.yml up -d --no-recreate --no-deps --scale {}={}".format(
            self.compose_path,
            self.container_name,
            amount_containers + scale)
        out = os.popen(cmd).read()
        Log.info(out)

    def start_init(self):
        cmd = "cd {} && docker-compose up -d".format(
            self.compose_path)
        out = os.popen(cmd).read()
        Log.info(out)
