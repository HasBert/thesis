import requests
import json
from logger import Log
from util import retry


class ElasticConnector:

    def __init__(self, elastic_ip, elastic_port):
        self._address = "http://{}:{}".format(elastic_ip, elastic_port)

    @retry(retry_times=20, sleep_time=5)
    def is_available(self):
        response = requests.get(self._address)
        if (response.status_code == 200):
            Log.info("Elastic route <{}> is now available!".format(self._address))
            return True
        else:
            raise Exception(
                "Route <{}> is NOT available, yet!".format(self._address))

    def _create_index(self, new_index):
        data = self._get_json_from_file('pre_reindex.json')
        Log.debug("Creating new index {}".format(new_index))
        response = requests.put("http://0.0.0.0:9200/{}".format(new_index), json=data)
        Log.info("Created new index {}".format(response.status_code))

    def _reindex(self, old_index, container, new_index):
        """
        This method is reindexing data from a given ip and applies data of the given apk
        to it. This function will be executed after an index is already created.
        """
        data = self._get_json_from_file('reindex.json')
        data["source"]["index"] = old_index
        data["source"]["query"]["bool"]["must"][0]["match"]["client_conn.address.host"] = container.ip
        data["dest"]["index"] = new_index
        data["script"]["params"]["apk"] = container.apk.__dict__
        Log.debug("Reindexing from {} to {}".format(old_index, new_index))
        response = requests.post("http://0.0.0.0:9200/_reindex", json=data)
        Log.info("Reindexing response status_code: {}".format(response.status_code))


    def _delete_from_old_index(self, old_index, container):
        data = self._get_json_from_file('delete_from_index.json')
        data["query"]["bool"]["must"][0]["match"]["client_conn.address.host"] = container.ip
        Log.debug("Deleting data from Index {}".format(old_index))
        response = requests.post("http://0.0.0.0:9200/{}/_delete_by_query".format(old_index), json=data)
        Log.info("Deleting response status_code: {}".format(response.status_code))




    def _get_json_from_file(self, filename):
        try:
            with open(filename) as json_file:
                return json.load(json_file)
        except Exception as error:
            Log.error("File {} not available".format(filename))

    def manipulate_index(self, old_index, container):
        new_index = "apk_{}".format(container.apk.id)
        self._create_index(new_index)
        self._reindex(old_index, container, new_index)
        self._delete_from_old_index(old_index, container)