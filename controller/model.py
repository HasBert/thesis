import re
import asyncio
from logger import Log



class Apk:
    def __init__(self, apk_info):
        self.path = apk_info["path"]
        self.id = apk_info["id"]
        self.name = apk_info["name"]
        self.version_code = apk_info["version_code"]
        self.version_name = apk_info["version_name"]

    def __repr__(self):
        return str(self.__dict__)

# create a parent class!
class ElasticContainer:
    container_name = "elastic"
    def __init__(self, container):
        self._container = container

    def __repr__(self):
        return "\n{{ip: {},\ncontainer_id: {},\napi_port: {},\nstarted_at: {}}}\n".format(
            self.ip, self.id, self.api_port, self.started_at)

    @property
    def container(self):
        return self._container

    @property
    def joined_network(self):
        return list(self._container.attrs['NetworkSettings']['Networks'].keys())[0]

    @property
    def ip(self):
        return self._container.attrs['NetworkSettings']['Networks'][
            self.joined_network]['IPAddress']

    @property
    def id(self):
        return self._container.id
    
    @property
    def api_port(self):
        return self._container.attrs['NetworkSettings']['Ports']['9200/tcp'][
            0]['HostPort']

    @property
    def started_at(self):
        return self._container.attrs["State"]["StartedAt"]

class AndroidContainer:
    container_name = "android-client"
    def __init__(self, container):
        self._container = container
        self._apk = None

    def __repr__(self):
        return "\n{{ip: {},\ncontainer_id: {},\nadb_port: {},\nstarted_at: {},\napk: {}}}\n".format(
            self.ip, self.id, self.adb_port, self.started_at, self._apk)

    def __eq__(self, other):
        return self.id == other.id

    @property
    def container(self):
        return self._container

    @property
    def adb_port(self):
        return self._container.attrs['NetworkSettings']['Ports']['5555/tcp'][
            0]['HostPort']

    @property
    def joined_network(self):
        return list(self._container.attrs['NetworkSettings']['Networks'].keys())[0]

    @property
    def ip(self):
        return self._container.attrs['NetworkSettings']['Networks'][
            self.joined_network]['IPAddress']

    @property
    def id(self):
        return self._container.id

    @property
    def started_at(self):
        return self._container.attrs["State"]["StartedAt"]

    @property
    def apk(self):
        return self._apk

    @apk.setter
    def apk(self, apk):
        self._apk = apk

    @property
    def health(self):
        return self._container.attrs["State"]["Health"]["Status"]

    def update(self):
        self._container.reload()

    async def health_check(self):
        while True:
            self._container.reload()
            if re.search("^healthy", self.health):
                Log.info("Container <{}> is <{}>".format(self.id, self.health))
                return
            elif re.search("^unhealthy", self.health):
                raise SystemError("Container {} is unhealthy!".format(self.health))
            Log.debug("container <{}> is <{}>".format(self.id, self.health))
            await asyncio.sleep(2)


# class ApkQueue(Queue, Observable):
#     """
#     This class is a child of Queue. The queue can handle threaded
#     read/write actions.
#     """
#     def __init__(self):
#         Queue.__init__(self)
#         Observable.__init__(self)
#         self.state = QueueState.EMPTY

#     def get(self, block=True, timeout=0):
#         try:
#             return super().get(block, timeout)
#         except Empty as empty_exception:
#             self.state = QueueState.EMPTY

#     def put(self, item, block=True, timeout=0):
#         try:
#             super().put(item, block, timeout)
#             self.state = QueueState.FILLED
#         except Full as full_exception:
#             self.state = QueueState.FULL