import abc
import time
from enum import Enum, auto
from logger import Log

def retry(retry_times=20, sleep_time=1):
    """
    Decorator to retry a passed in function. The function needs to throw an exception
    if retry should be executed again.
    """
    retries = retry_times

    def decorator(func):
        def wrapper(*args, **kwargs):
            nonlocal retries
            result = None
            try:
                result = func(*args, **kwargs)
            except Exception as err:
                Log.error("Error during execution of function {} occured:\n{}".format(
                    func.__name__, err))
                if retries > 0:
                    retries -= 1
                else:
                    retries = retry_times
                    return result
                Log.debug("Retrying execution of function <{}> for <{}> more times with <{}>s delay".format(
                    func.__name__, retries, sleep_time))
                time.sleep(sleep_time)
                result = wrapper(*args, **kwargs)
            retries = retry_times
            return result
        return wrapper
    return decorator


class QueueState(Enum):
    FULL = auto()
    EMPTY = auto()
    FILLED = auto()


class Observable(object):
    """
    The Observable let Observers assign to it, those Observers will be
    notified if it state changes.
    """
    def __init__(self):
        self._observers = set()
        self._state = None

    def attach(self, observer):
        #observer._subject = self
        self._observers.add(observer)
        Log.debug("Attached observer of instance: {}".format(
            type(observer).__name__))

    def detach(self, observer):
        #observer._subject = None
        self._observers.discard(observer)
        Log.debug("Detached observer of instance: {}".format(
            type(observer).__name__))

    def _notify(self):
        for observer in self._observers:
            observer.update(self._state)

    @property
    def state(self):
        return self._state

    @state.setter
    def state(self, state):
        self._state = state
        self._notify()


class Observer(object, metaclass=abc.ABCMeta):
    """
    Define an updating interface for objects that should be notified of
    changes in a subject.
    """
    def __init__(self):
        self._subject = None
        self._observer_state = None

    @abc.abstractmethod
    def update(self, arg):
        raise NotImplementedError(
            'users must define <update> to use this base class')