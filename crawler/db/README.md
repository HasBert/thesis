# App-Check databases

This repository contains the scripts needed to setup the databases used in
the app-check project.

## Prerequisites

The scripts are designed for the *Percona MySQL database engine*. If you want
to deploy them to a different SQL server like PostgreSQL or MariaDB, you
might have to change small portions of the scripts.

Log into the database server as administrator and create a user account for
applications first:

```mysql
create user appcheck identified by '<password>';
```

You can choose any password for the user, but if you change the username
from `appcheck` to something else, you have to adjust the database scripts
accordingly.

## Creating the databases

### APP

```bash
mysql -u root -p < create_APP.sql
```

