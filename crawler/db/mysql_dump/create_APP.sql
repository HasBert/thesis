-- ============================================================================
-- [Project APP-CHECK] Create APP database (Percona)
-- ============================================================================
-- Author: Bernd Fix   >Y<
--
-- This program is free software: you can redistribute it and/or modify it
-- under the terms of the GNU Affero General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or (at
-- your option) any later version.
--
-- This program is distributed in the hope that it will be useful, but
-- WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
-- or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public
-- License for more details.
--
-- You should have received a copy of the GNU Affero General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.


-- Create a new APP database instance
drop database if exists APP;
create database APP character set utf8mb4 collate utf8mb4_unicode_ci;
use APP;

-- Allow (unrestricted) access to the database for the 'appcheck' user 
-- grant all on APP.* to appcheck;
-- flush privileges;


-- =============================================================================
-- APP TABLES
-- =============================================================================

-- Table for App categories
-- Due to the harvest method (using GooglePlayAPI and play_scraper in parallel
-- to maximize the amount of metadata retrieved) the main category (as set by
-- GPA) is a direct reference from the app record. Categories delivered by
-- play_scraper are mapped to apps in the separate table app_cats (see below). 
create table category (
	id				integer auto_increment primary key,
	name			varchar(64)
);


-- Table for App types
-- GPA delivered info about the overall type of app. Referenced directly in
-- the associated app record.
create table type (
	id				integer auto_increment primary key,
	name			varchar(64)
);


-- Table for app metadata retrieved from Google Play Store and internal states.
-- It serves as the main reference to apps managed by the app-check project.
create table app (
	-- internal organization
	id				integer auto_increment primary key,
	status			integer default 0,						-- 0: pending (no version binary yet),
															-- 1: registered (fully harvested)
															-- 2: updated (trigger test cycle)
															-- 3: manual interaction required 
	selected		boolean	default false,					-- app included in test list?
	last_check		timestamp default current_timestamp,	-- time of last update check
	check_period	integer default 7,						-- update check period (in days)
	last_test		timestamp default null,					-- time of last (successful) test cycle run
	test_runs		integer default 0,						-- number of (successful) test runs
	emulator		boolean default false,					-- can use emulator for test run?
	
	-- metadata from Google Play Store
	doc_id			varchar(255) not null,					-- unique reference to app
	current_version	integer default null,					-- reference to current version
	app_category	integer,								-- [GPA] main category
	app_type		integer,								-- [GPA] app class
	contains_ads	boolean,								-- app shows ads?
	downloads		bigint,									-- lower bound for number of downloads
	title			varchar(64) not null,					-- name of app
	description		varchar(8192),							-- description of app
	author			varchar(64) not null,					-- developer / author
	ratings			integer,								-- number of ratings
	rating_type		integer,								-- rating class
	comments		integer,								-- number of comments
	score			float,									-- review score (if reviewed at all) 
    star_1			integer,								-- number of "one star" ratings
    star_2			integer,								-- number of "two stars" ratings
    star_3			integer,								-- number of "three stars" ratings
    star_4			integer,								-- number of "four stars" ratings
    star_5			integer,								-- number of "five stars" ratings

	unique key(doc_id),
	foreign key(app_category) references category(id), 
	foreign key(app_type) references type(id) 
);

-- Table for app version (1:n relationship with app record). The current
-- version for an app is referenced directly in the app record (field
-- "current_version"). All version instances have a back-reference to the
-- associated app.
create table version (
	id				integer auto_increment primary key,
	app				integer not null,						-- reference to app
	tested			boolean default false,					-- app version tested? 
	version_code	integer not null,						-- unique reference to version
	version_string	varchar(64),							-- human-readable version string
	upload_date		date,									-- date of upload
	inst_size		integer,								-- size of binary in bytes
	unstable		boolean,								-- version considered unstable?
	recent_changes	varchar(1024),							-- description of recent changes
	apk				varchar(64) default null				-- name of APK file
);

-- Cross-references between app and associated versions.
alter table app add foreign key (current_version) references version(id) on delete set null;
alter table version add foreign key (app) references app(id);


-- Table for files associated with a version
create table file (
	id				integer auto_increment primary key,
	type			integer,								-- file type
	size			integer,								-- file size in bytes
	version			integer									-- version code
);

-- Association table for version-related files
create table version_files (
	version			integer,								-- reference to app version
	file			integer,								-- reference to file

	foreign key(version) references version(id),
	foreign key(file) references file(id) on delete cascade
);


-- Association table for app dependencies
create table dependencies (
	app_version		integer not null,						-- reference to app
	sub_version		integer not null,						-- reference to dependency

	foreign key (app_version) references version(id),
	foreign key (sub_version) references version(id)
);


-- Association table for app categories (see table category for details)
create table app_cats (
	app				integer,								-- reference to app
	cat				integer,								-- reference to category

	foreign key (app) references app(id),
	foreign key (cat) references category(id)
);


-- Table for app permissions
create table permission (
	id				integer auto_increment primary key,
	name			varchar(512)
);

-- Association table for app permissions
create table app_perms (
	app				integer,								-- reference to app
	perm			integer,								-- reference to permission

	foreign key (app) references app(id),
	foreign key (perm) references permission(id)
);


-- Table for images related to an app
create table image (
	id				integer auto_increment primary key,
	url				varchar(255) not null,					-- image url
	fife_options	boolean,								-- fifeUrlOptions supported?
	height			integer,								-- height of image
	width			integer,								-- width of image
	type			integer,								-- image type

	unique key(url)
);

-- Association table for images
create table app_imgs (
	app				integer,								-- reference to app
	img				integer,								-- reference to image

	foreign key (app) references app(id),
	foreign key (img) references image(id) on delete cascade
);


-- Table for app rating
create table rating (
	id              integer auto_increment primary key,
	name            varchar(64)
);

-- Association table for app ratings
create table app_ratings (
	app             integer,								-- reference to app
	rate            integer,								-- reference to ratings

	foreign key (app) references app(id),
	foreign key (rate) references rating(id)
);


-- Table for interactive app elements
create table element (
	id              integer auto_increment primary key,
	name            varchar(64)
);

-- Association table for interactive app elements
create table app_elements (
	app             integer,								-- reference to app
	elem            integer,								-- reference to element description

	foreign key (app) references app(id),
	foreign key (elem) references element(id)
);


-- Table for offers (relates to an app version)
create table offer (
	id				integer auto_increment primary key,
	checkout		boolean,								-- checkout flow required?
	sale_end		date,									-- end date for sale
	currency		varchar(8),								-- currency code
	amount			varchar(16),							-- formatted amount
	micros			integer,								--
	type			integer									-- offer type
);


-- Association table for versions and permissions
create table version_permission (
	version_id			integer,								-- reference to version
	perm						varchar(512),						-- reference to permission

	foreign key (version) references version(id),
	foreign key (offer) references offer(id) on delete cascade
);


-- Association table for offers
create table version_offers (
	version			integer,								-- reference to version
	offer			integer,								-- reference to offer

	foreign key (version) references version(id),
	foreign key (offer) references offer(id) on delete cascade
);


-- =============================================================================
-- APP STORED PROCEDURES
-- =============================================================================

delimiter //
//

-- Remove a version from the database: removes all associated entries that are
-- referenced by deleted objects (if no other references are present).
-- Returns true if version was successfully dropped
drop procedure if exists drop_version//
create procedure drop_version(in ver_id integer, out ok boolean)
$0: begin
	-- cancel removal if versions of other apps depend on this version
	declare num_deps integer;
	select count(*) into num_deps from dependencies where sub_version = ver_id;
	set ok = true;
	if num_deps > 0 then
		set ok = false;
		leave $0;
	end if;

	-- remove associated data:
	-- (1) dependencies on versions of other apps
	delete from dependencies where app_version = ver_id;
	-- (2) files associated with a version
	delete from version_files where version = ver_id;
	-- (3) offers for a version
	delete from version_offers where version = ver_id;

	-- remove version record
	delete from version where id = ver_id;
end//


-- Remove an app from the database: removes all associated versions and entries
-- that are referenced by deleted objects (if no other references are present)
drop procedure if exists drop_app//
create procedure drop_app(in app_id integer, out rc integer)
$0: begin
	-- remove associated data:
	-- (1) drop all app versions
	declare done integer default 0;
	declare ver_id integer;
	declare ok boolean;
	declare crs_version cursor for
		select id from version where app = app_id;
	declare continue handler for not found set done = 1;
	
	start transaction;
	update app set current_version = null where id = app_id;
	open crs_version;
	$1: loop
		fetch crs_version into ver_id;
		if done = 1 then
			leave $1;
		end if;
		-- drop version; rollback if version could not be deleted.
		call drop_version(ver_id, ok);
		if not ok then
			rollback;
			leave $0;
		end if;
	end loop $1;
	set done = 0;

	-- (2) remove category references; keep categories
	delete from app_cats where app = app_id;
	-- (3) remove permissions
	delete from app_perms where app = app_id;
	-- (4) remove images
	delete from app_imgs where app = app_id;
	-- (5) remove rating references; keep ratings
	delete from app_ratings where app = app_id;
	-- (6) remove element references; keep interactive elements
	delete from app_elements where app = app_id;
	-- (7) remove app record
	delete from app where id = app_id;

	-- check for success
	if done = 1 then
		rollback;
		leave $0;
	end if;
	commit;
end//

delimiter ;
