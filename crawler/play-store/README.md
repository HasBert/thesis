
# [app-check] Interaction with Google Play Store

This repository is part of the App-Check project - see https://app-check.org
for more details. The project aims to automate the testing of Android apps
with special focus on privacy and data protection issues.

Software in this repository interacts with the Google Play Store to extract
app meta data (e.g. version, permissions, categories, number of download and
user ratings) for popular apps and to download app binaries (APK files).

All data relating to apps is stored in a database (schema APP in a MySQL
instance). The setup for the APP schema can be foound in the file
`create_APP.sql` in the `database` repository.

## License

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or (at
your option) any later version.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.

## Prerequisites

To use the applications in this repositories you need to:

### Setup the APP database instance

For more details see repository `database` in group `app-check`.

### Provide Google Play Store credentials

Pass the credentials to log into the Google Play Store by setting environment
variables before running the program:

```bash
export GPS_USER=<username>
export GPS_PASSWD=<password>
export GPS_LOCALE=<locale>
export GPS_DEVICE=<device>
```

* `locale` defines the language/territory combination as defined by ISO/IEC
15897 (but without the codeset or modifier extensions). For Germany, the
locale would be `de_DE`, the default locale is `en_US`.
* `device` represents a valid phone identifier as defined by Google Android.
The default device is `walleye` (Google Pixel2).


### Provide database credentials

Pass the credentials to log into the database at a given host by setting
environment variables before running the program:

```bash
export DB_USER=<username>
export DB_PASSWD=<password>
export DB_HOST=<hostname>
```

Please see the remarks in the `database` repository.

## Stand-alone applications

### crawler.py

Crawl the Google Play Store for app ids. Use this option to
initially fill the APP database with 40000+ apps.

The following command-line options are available:

* `-o <file>`: Output the app ids to file.

* `-s`: Skip apps already stored in the database.

* `-l`: Locale to use for crawling (default is "de_DE")

If using the `-s` option, the environment variables for the database
credentials need to be set (see above).

### harvest.py

Harvest the Google Play Store for app metadata. The following command-line options
are available:

* `-i <app_id>`: Add a single app to the database. The app_id is a unique string
identifying the app (e.g. `org.mozilla.firefox_beta`).

* `-f <app_id_file>`: Add apps to the database. The file contains a list of app_ids
(one entry per line) that will be processed.

* `-d`: Switch mode: Instead of harvesting apps, the program will *delete* all apps
specified by the `-i`, `-f` or `-s` option from the database. Use with care.

If multiple app_ids are processed (`-f` option), the program will output two
files when it is done:

* `fail-ids.txt` contains a list of app_ids that did not process correctly
* `success-ids.txt` contains a list of app_ids that did process correctly
