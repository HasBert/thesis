#!/usr/bin/python3

"""
# ============================================================================
# [Project APP-CHECK] Crawl for apps on "Google Play Store"
# ============================================================================
# Author: Bernd Fix   >Y<
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or (at
# your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import sys
import os
import getopt
import time
from collections import deque
from requests.exceptions import HTTPError
import mysql.connector
from mysql.connector.errors import InterfaceError, ProgrammingError
import play_scraper


def next_category():
    '''
    iterate categories (with age range specifier for FAMILY category)
    '''
    for category in play_scraper.lists.CATEGORIES:
        if category == "FAMILY":
            for age in play_scraper.lists.AGE_RANGE:
                yield category, age, category + ": " + age
        else:
            yield category, None, category


def get_app_id(doc_id, app_db):
    '''
    Returns the database id for a given app (doc_id).
    '''
    crs = app_db.cursor()
    crs.execute("SELECT id FROM app WHERE doc_id = %s", (doc_id,))
    result = crs.fetchone()
    if result:
        return result[0]
    return None


def write_ids(id_list, name):
    '''
    Write id list to file
    '''
    with open(name, 'w') as file:
        for id_str in id_list:
            file.write("%s\n" % id_str)


def run_crawler(app_db, file, locale, skip):
    '''
    Crawl the Google Play Store for apps we don't know yet.
    The list of discovered app ids is written to a file.
    '''
    # Collect from lists of top-500 apps in all categories and collections.
    queue = deque()
    loc = locale.lower().split("_")
    print("Crawl the Google Play Store for popular apps:")
    for category, age, tag in next_category():
        print("\tProcessing category '{}'...".format(tag))
        for collection in ("TOP_FREE", "TOP_PAID", "TOP_GROSSING", "TRENDING"):
            print("\t\tProcessing collection '{}'...".format(collection))
            for page in range(0, 5):
                print("\t\t\tFetching next 100 entries...")
                # fetch next block from the list (retry 5 times)
                for retry in range(0, 5):
                    top = []
                    try:
                        top = play_scraper.collection(
                            category=category,
                            collection=collection,
                            results=100,
                            page=page,
                            hl=loc[0],
                            gl=loc[1],
                            age=age
                        )
                        break
                    except HTTPError as exc:
                        print("ERROR: Failed to retrieve collection list")
                        if exc.response.status_code == 404:
                            break
                        time.sleep(2**retry)
                        print("Retrying {}/5...".format(retry+1))

                # add app_ids to queue
                num = len(top)
                for app in top:
                    app_id = app["app_id"]
                    if queue.count(app_id) == 0:
                        if not skip or get_app_id(app_id, app_db) is None:
                            queue.append(app_id)
                if num > 0:
                    print("\t\t\tFound {} apps, {} total".format(num, len(queue)))
                if num < 100:
                    break

    # save queued app_ids
    write_ids(queue, file)
    print("Stats: {} apps crawled.".format(len(queue)))


def usage():
    """
    Show help on how to use the program
    """
    print("crawler.py -o <id_file> [-s] [-l <locale>]")
    print("   -o: list of crawled app_ids")
    print("   -s: skip discovered app_id if already in database")
    print("   -l: locale to use (default: 'de_DE')")


def main(argv):
    """
    application entry point
    """
    # parse command line options
    id_file = ''
    skip = False
    locale = "de_DE"
    db_user = os.environ["DB_USER"]
    db_passwd = os.environ["DB_PASSWD"]
    db_host = os.environ["DB_HOST"]
    try:
        opts, _ = getopt.getopt(argv, "so:l:")
    except getopt.GetoptError:
        usage()
        sys.exit(1)
    for opt, arg in opts:
        if opt == '-o':
            id_file = arg
        elif opt == "-s":
            skip = True
        elif opt == "-l":
            locale = arg

    app_db = None
    if skip:
        # connect to APP database
        try:
            app_db = mysql.connector.connect(
                host=db_host,
                database="APP",
                charset="utf8mb4",
                collation="utf8mb4_unicode_ci",
                user=db_user,
                password=db_passwd
            )
        except InterfaceError:
            print("ERROR: Database connection failed")
            sys.exit(2)
        except ProgrammingError:
            print("ERROR: Database login failed")
            sys.exit(2)

    # process app id(s)
    if len(id_file) > 0:
        run_crawler(app_db, id_file, locale, skip)
    else:
        # unknown or missing options
        print("Missing argument:")
        usage()
        sys.exit(1)


if __name__ == "__main__":
    main(sys.argv[1:])
