#!/usr/bin/python3

"""
# ============================================================================
# [Project APP-CHECK] Harvest app metadata from "Google Play Store"
# ============================================================================
# Author: Bernd Fix   >Y<
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or (at
# your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public
# License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import sys
import os
import getopt
import traceback
import dateparser
from dateparser.search import search_dates
import mysql.connector
import play_scraper
from gpapi.googleplay import GooglePlayAPI, RequestError, LoginError, SecurityCheckError


def next_category():
    '''
    iterate categories (with age range specifier for FAMILY category)
    '''
    for category in play_scraper.lists.CATEGORIES:
        if category == "FAMILY":
            for age in play_scraper.lists.AGE_RANGE:
                yield age, category
        else:
            yield None, category


def get_app_id(doc_id, app_db):
    """
    Returns the database id for a given app (doc_id).
    """
    crs = app_db.cursor()
    crs.execute("SELECT id FROM app WHERE doc_id = %s", (doc_id,))
    result = crs.fetchone()
    if result:
        return result[0]
    return None


def get_entry(info, key):
    """
    Get an entry from a dict or None if the entry does not exist
    """
    return info[key] if key in info else None


def get_version_id(app_id, info, app_db, locale):
    """
    Returns the database id for a given version of an app (id).
    If the entry does not exists, it is automatically created.
    """
    version_code = get_entry(info, "versionCode")
    if not version_code:
        return None, False

    crs = app_db.cursor()
    crs.execute(
        "SELECT id FROM version WHERE version_code = %s AND app = %s",
        (version_code, app_id)
    )
    result = crs.fetchone()
    if result is None:
        size = get_entry(info, "installationSize")
        if size and size > 2147483647:
            size = None
        udate = get_entry(info, "uploadDate")
        upld = get_date(udate, locale) if udate else None
        crs = app_db.cursor()
        crs.execute(
            """
            INSERT INTO version(
                version_code,version_string,app,upload_date,inst_size,unstable,recent_changes
            ) VALUES(%s,%s,%s,%s,%s,%s,%s)
            """,
            (version_code, get_entry(info, "versionString"), app_id, upld, size,
             get_entry(info, "unstable"), get_entry(info, "recentChanges"))
        )
        return crs.lastrowid, True
    return result[0], False


def get_id(name, table, app_db):
    """
    Returns the database id for a named entry in a specified table.
    If the entry does not exists, it is automatically created.
    """
    if not name:
        return None
    crs = app_db.cursor()
    crs.execute("SELECT id FROM " + table + " WHERE name = %s", (name,))
    result = crs.fetchone()
    if result is None:
        crs = app_db.cursor()
        crs.execute("INSERT INTO " + table + "(name) VALUES(%s)", (name,))
        return crs.lastrowid
    return result[0]


def set_references(names, table, version_id, ref_id, table_ref, app_db):
    """
    Traverse a list of names associated with a version id (from a separate
    table). The references between the names and app_id are stored in table_ref.
    """
    if names:
        for name in names:
            name_id = get_id(name, table, app_db)
            crs = app_db.cursor()
            crs.execute(
                "INSERT INTO " + table_ref +
                "(version," + ref_id + ") VALUES(%s,%s)""",
                (version_id, name_id)
            )


def add_file(version_id, file, app_db):
    """
    Add a file to version (bundle)
    """
    crs = app_db.cursor()
    crs.execute(
        "INSERT INTO file(type,size,version) VALUES(%s,%s,%s)",
        (file["fileType"], file["size"], file["version"])
    )
    file_id = crs.lastrowid
    crs = app_db.cursor()
    crs.execute(
        "INSERT INTO version_files(version,file) VALUES(%s,%s)",
        (version_id, file_id)
    )


def add_image(app_id, img, app_db):
    """
    Add ab image to app (description). It is only added if previously unknown.
    Images are uniquely identified by thier URL.
    """
    crs = app_db.cursor()
    crs.execute("SELECT id FROM image WHERE url = %s", (img["url"],))
    if not crs.fetchone():
        crs = app_db.cursor()
        crs.execute(
            """
            INSERT INTO image(url,fife_options,height,width,type)
            VALUES(%s,%s,%s,%s,%s)
            """,
            (img["url"], img["supportsFifeUrlOptions"], img["height"],
             img["width"], img["imageType"])
        )
        img_id = crs.lastrowid
        crs = app_db.cursor()
        crs.execute(
            "INSERT INTO app_imgs(app,img) VALUES(%s,%s)", (app_id, img_id)
        )


def add_dependency(version_id, dep, app_db, api, locale, depth):
    """
    Add dependencies to app version.
    This call is recursively resolving dependencies by harvesting them if they
    not already available in the database.
    """
    # collect info about dependency
    info_dep = {
        "versionCode": dep["version"],
        #       "installationSize": null,
        #       "uploadDate": null,
        #       "versionString": null,
        #       "unstable": null,
        #       "recentChanges": null,
    }
    # find reference to dependency; harvest it if necessary
    sub = dep["packageName"]
    count = 0
    sub_id = get_app_id(sub, app_db)
    if not sub_id:
        sub_id, num = harvest(sub, app_db, api, locale, depth)
        count += num
    sub_version_id, _ = get_version_id(sub_id, info_dep, app_db, locale)
    if sub_version_id:
        # store dependency in database
        crs = app_db.cursor()
        crs.execute(
            "INSERT INTO dependencies(app_version,sub_version) VALUES(%s,%s)",
            (version_id, sub_version_id)
        )
    return count


def add_offer(version_id, offer, app_db, locale):
    """
    Add  offer for an app version
    """
    # add offer to database
    crs = app_db.cursor()
    crs.execute(
        """
        INSERT INTO offer(checkout,sale_end,currency,amount,micros,type)
        VALUES(%s,%s,%s,%s,%s,%s)
        """,
        (offer["checkoutFlowRequired"], get_end_of_sale(offer["saleEnds"], locale),
         offer["currencyCode"], offer["formattedAmount"], offer["micros"], offer["offerType"])
    )
    offer_id = crs.lastrowid

    # link offer to version
    crs = app_db.cursor()
    crs.execute(
        "INSERT INTO version_offers(version,offer) VALUES(%s,%s)",
        (version_id, offer_id)
    )


def estimate_downloads(dwnl_str):
    """
    Estimate the lower bound for number of downloads
    """
    return None if not dwnl_str else int(''.join(filter(str.isdigit, dwnl_str)))


def write_ids(app_ids, name):
    """
    Write app_id list to file
    """
    with open(name, 'w') as file:
        for app_id in app_ids:
            file.write("%s\n" % app_id)


def get_date(date_str, locale):
    """
    Get date from metadata string formated in a specific locale
    """
    if not date_str:
        return None
    lang = locale.split("_")[0]
    return dateparser.parse(date_str, languages=[lang])


def get_end_of_sale(period, locale):
    """
    Compute "end of sale" timestamp
    """
    if period:
        lang = locale.split("_")[0]
        end_date = search_dates(period, languages=[lang])
        if end_date:
            return end_date[0][1]
    return None


def drop_app(doc_id, app_db):
    """
    Remove an app from the database
    """
    app_id = get_app_id(doc_id, app_db)
    if app_id:
        crs = app_db.cursor()
        args = [app_id, 0]
        result_args = crs.callproc('drop_app', args)
        if not result_args[1]:
            print("drop_app({}) failed: Internal".format(doc_id))
    else:
        print("drop_app({}) failed: not found".format(doc_id))


def harvest(doc_id, app_db, api, locale, depth=0):
    """
     harvest() takes a single app (doc_id) and retrieves all metadata for the app
     from the Google Play Store. Selected metadata is stored into the APP database
     for further processing. The database entry of an app is updated whenever its
     doc_id is harvested again at a later time.
     The function returns the database id of the app table record that was
     inserted or updated and a count for number of harvested apps.
    """
    print("{}harvesting '{}'".format(" "*(3*(depth+1)), doc_id))

    # Download app metadata from Google Play Store
    try:
        details = api.details(doc_id)
        loc = locale.lower().split("_")
        details2 = play_scraper.details(doc_id, loc[0], loc[1])
    except RequestError as exc:
        print("ERROR: GooglePlayAPI request failed:", exc)
        return None, 0
    except Exception as exc:
        print("ERROR: Metadata retrieval failed:", exc)
        exc_type, _, _ = sys.exc_info()
        print("   Execption type:", exc_type)
        return None, 0

    # sanity check: retrieved doc_id matches argument?
    # TODO: Any other checks required?
    if doc_id != details["docId"]:
        print("ERROR: Metadata retrieval inconsistent")
        print("   doc_id mismatch ({} != {})".format(doc_id, details["docId"]))
        return None, 0

    # Store/update metadata in APP database
    count = 1
    try:
        # prepare metadata
        rating = details["aggregateRating"]
        dwnl = estimate_downloads(details["numDownloads"])
        val = (
            doc_id, details["containsAds"] == "Contains ads", dwnl,
            details["title"], details["description"], details["author"],
            rating["ratingsCount"], rating["commentCount"], rating["starRating"],
            rating["oneStarRatings"], rating["twoStarRatings"], rating["threeStarRatings"],
            rating["fourStarRatings"], rating["fiveStarRatings"], rating["type"]
        )

        # Because MySQL does not permit nested transaction, the recursive harvesting
        # of dependencies for an app can only be processed as a single transaction
        # that is started at depth 0 of the recursion.
        # TODO: Check if problem can be solved by a SAVEPOINT statement
        #       before entering the recursion.
        if depth == 0:
            app_db.start_transaction()
        crs = app_db.cursor()

        # check if app is already stored in the database
        app_id = get_app_id(doc_id, app_db)
        if not app_id:
            # ------- store metadata
            crs.execute(
                """
                INSERT INTO app (
                    doc_id, contains_ads, downloads, title, description, author, ratings,
                    comments, score, star_1, star_2, star_3, star_4, star_5, rating_type
                ) VALUES (
                    %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s
                )
                """, val)
            app_id = crs.lastrowid
        else:
            # ------- update metadata
            crs.execute(
                """
                UPDATE app SET last_check = current_timestamp,
                doc_id = %s, contains_ads = %s, downloads = %s, title = %s, description = %s,
                author = %s, ratings = %s, comments = %s, score = %s, star_1 = %s,
                star_2 = %s,star_3 = %s,star_4 = %s,star_5 = %s, rating_type = %s
                """, val)

        # ------ process app version (maybe updated since last check)
        version_id, is_new = get_version_id(app_id, details, app_db, locale)

        # only if a new version is encountered, the associated files, dependencies,
        # ratings, permissions, categories, interactive elements and offers are
        # added to the database.
        if is_new:
            # set back reference to app (as current version)
            crs = app_db.cursor()
            crs.execute(
                "UPDATE app SET current_version = %s WHERE id = %s", (
                    version_id, app_id)
            )
            # add dependencies for new version (recursive!)
            for dep in details["dependencies"]:
                count += add_dependency(version_id, dep,
                                        app_db, api, locale, depth+1)

            # add files for new version
            for file in details["files"]:
                add_file(version_id, file, app_db)

            # add offers for new version
            for offer in details["offer"]:
                add_offer(version_id, offer, app_db, locale)

            # add permissions for new version
            set_references(
                details["permission"], "permission", version_id, "perm",
                "version_permissions", app_db
            )
            set_references(
                details2["category"], "category", version_id, "cat",
                "version_categories", app_db
            )

            # add ratings for new version
            ratings = ()
            rate_list = details2["content_rating"]
            if rate_list:
                for rates in rate_list:
                    for rate in rates.split(','):
                        if len(rate) > 64:
                            rate = rate[:64]
                        ratings += (rate.strip(),)
                set_references(ratings, "rating", version_id,
                               "rate", "version_ratings", app_db)

            # add interactive elements for this version
            elements = ()
            interactive = details2["interactive_elements"]
            if interactive:
                for elem_str in interactive:
                    for elem in elem_str.split(','):
                        elements += (elem.strip(),)
                set_references(elements, "element", version_id,
                               "elem", "version_elements", app_db)

        # ------ store image references for app
        for img in details["images"]:
            add_image(app_id, img, app_db)

        # only commit if we are at depth 0 of a recursion
        if depth == 0:
            app_db.commit()

        # return the id the harvested app and the number of harvested apps
        # (can be >1 if dependencies were resolved in the process)
        return app_id, count

    except Exception as exc:
        print("ERROR: Metadata processing failed:", exc)
        exc_type, _, _ = sys.exc_info()
        print("   Execption type:", exc_type)
        app_db.rollback()
        return None, 0


def usage():
    """
    Show help on how to use the program
    """
    print("harvest.py -f <id_file> [-d] [-t]")
    print("   -f|--idfile= : process list of app_ids FROM file")
    print("   -d : Modifier to remove apps instead of harvesting them")
    print("   -t : Test if apps are included in the database")
    print("harvest.py -i <app_id> [-d] [-t]")
    print("   -i|--appid= : process a single app_id")


def main(argv):
    """
    application entry point
    """
    # parse command line options
    app_id = ''
    id_file = ''
    gps_user = os.environ["GPS_USER"]
    gps_passwd = os.environ["GPS_PASSWD"]
    gps_locale = os.environ["GPS_LOCALE"]
    if gps_locale is None:
        gps_locale = "en_US"
    gps_device = os.environ["GPS_DEVICE"]
    if gps_device is None:
        gps_device = "walleye"
    db_user = os.environ["DB_USER"]
    db_passwd = os.environ["DB_PASSWD"]
    db_host = os.environ["DB_HOST"]
    delete = False
    check = False
    try:
        opts, _ = getopt.getopt(argv, "dtf:i:", ["idfile=", "appid="])
    except getopt.GetoptError:
        usage()
        print()
        sys.exit(1)
    for opt, arg in opts:
        if opt == '-d':
            delete = True
        elif opt == '-t':
            check = True
        elif opt in ('-f', '--idfile'):
            id_file = arg
        elif opt in ('-i', '--appid'):
            app_id = arg

    # connect to APP database
    try:
        app_db = mysql.connector.connect(
            host=db_host,
            database="APP",
            charset="utf8mb4",
            collation="utf8mb4_unicode_ci",
            user=db_user,
            password=db_passwd
        )
    except Exception as exc:
        print("ERROR: Database connection failed:", exc)
        exc_type, _, _ = sys.exc_info()
        print("   Execption type:", exc_type)
        sys.exit(2)

    # log into Google Play Store
    try:
        api = GooglePlayAPI(locale=gps_locale, timezone="UTC",
                            device_codename=gps_device)
        api.login(email=gps_user, password=gps_passwd)
    except LoginError as exc:
        print("ERROR: Google Play Store login failed:\n {}").format(exc)
        sys.exit(3)
    except SecurityCheckError as secExc:
        print(
            "ERROR: Google Play Store login produced SecurityCheckError:\n {}").format(secExc)
        sys.exit(3)
    except Exception as exc:
        print("ERROR:", traceback.format_exc())
        sys.exit(4)

    # process app id(s)
    if id_file:
        # process apps defined in file
        print("Processing APP_IDs from file '" + id_file + "'")
        success = []
        fail = []
        with open(id_file, "r") as file:
            doc_ids = [line.strip() for line in file.readlines()]
            for doc_id in doc_ids:
                if delete:
                    drop_app(doc_id, app_db)
                else:
                    if check:
                        app_id = get_app_id(doc_id, app_db)
                    else:
                        app_id, _ = harvest(doc_id, app_db, api, gps_locale)
                    if app_id:
                        success.append(doc_id)
                    else:
                        fail.append(doc_id)
        if not delete:
            write_ids(success, "success-ids.txt")
            write_ids(fail, "fail-ids.txt")
    elif app_id:
        # process single app directly
        print("Processing single APP_ID:")
        if delete:
            drop_app(app_id, app_db)
        elif check:
            print(get_app_id(doc_id, app_db))
        else:
            harvest(app_id, app_db, api, gps_locale)
    else:
        # unknown or missing options
        print("Missing argument:")
        usage()
        sys.exit(1)


if __name__ == "__main__":
    main(sys.argv[1:])
