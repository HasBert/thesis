"""
This script serializes the entire traffic dump, including websocket traffic,
as JSON, and either sends it to a URL or writes to a file. The serialization
format is optimized for Elasticsearch; the script can be used to send all
captured traffic to Elasticsearch directly.
Usage:
    mitmproxy
        --mode reverse:http://example.com/
        -s examples/complex/jsondump.py
Configuration:
    Send to a URL:
        cat > ~/.mitmproxy/config.yaml <<EOF
        dump_destination: "https://elastic.search.local/my-index/my-type"
        # Optional Basic auth:
        dump_username: "never-gonna-give-you-up"
        dump_password: "never-gonna-let-you-down"
        # Optional base64 encoding of content fields
        # to store as binary fields in Elasticsearch:
        dump_encodecontent: true
        EOF
    Dump to a local file:
        cat > ~/.mitmproxy/config.yaml <<EOF
        dump_destination: "/user/rastley/output.log"
        EOF
"""
from threading import Lock, Thread
from queue import Queue
import base64
import json
import requests
import traceback

from mitmproxy import ctx
from mitmproxy import exceptions
from mitmproxy.net.http import encoding

FILE_WORKERS = 1
HTTP_WORKERS = 10

DEFAULT_DUMP_DESTINATION = "jsondump.out"
DEFAULT_INDEX_NAME = "/none"


class JSONDumper:
    """
    JSONDumper performs JSON serialization and some extra processing
    for out-of-the-box Elasticsearch support, and then either writes
    the result to a file or sends it to a URL.
    """

    def __init__(self):
        self.outfile = None
        self.transformations = None
        self.encode = None
        self.url = None
        self.lock = None
        self.auth = None
        self.queue = Queue()

    def done(self):
        self.queue.join()
        if self.outfile:
            self.outfile.close()

    fields = {
        'timestamp': (
            ('error', 'timestamp'),

            ('request', 'timestamp_start'),
            ('request', 'timestamp_end'),

            ('response', 'timestamp_start'),
            ('response', 'timestamp_end'),

            ('client_conn', 'timestamp_start'),
            ('client_conn', 'timestamp_end'),
            ('client_conn', 'timestamp_tls_setup'),

            ('server_conn', 'timestamp_start'),
            ('server_conn', 'timestamp_end'),
            ('server_conn', 'timestamp_tls_setup'),
            ('server_conn', 'timestamp_tcp_setup'),
        ),
        'ip': (
            ('server_conn', 'source_address'),
            ('server_conn', 'ip_address'),
            ('server_conn', 'address'),
            ('client_conn', 'address'),
        ),
        'cert': (
            ('client_conn', 'mitmcert'),
            ('server_conn', 'cert'),
        ),
        'extension': (
            ('client_conn', 'tls_extensions'),
        ),
        'ws_messages': (
            ('messages', ),
        ),
        'headers': (
            ('request', 'headers'),
            ('response', 'headers'),
        ),
        'content': (
            ('request', 'content'),
            ('response', 'content'),
        ),
    }

    def _init_transformations(self):
        self.transformations = [
            {
                'fields': self.fields['headers'],
                'func': dict,
            },
            {
                'fields': self.fields['timestamp'],
                'func': lambda t: int(t * 1000),
            },
            {
                'fields': self.fields['ip'],
                'func': lambda addr: {
                    'host': addr[0].replace('::ffff:', ''),
                    'port': addr[1],
                },
            },
            {
                'fields': self.fields['ws_messages'],
                'func': lambda ms: [{
                    'type': m[0],
                    'from_client': m[1],
                    'content': base64.b64encode(bytes(m[2], 'utf-8')) if self.encode else m[2],
                    'timestamp': int(m[3] * 1000),
                } for m in ms],
            },
            {
                'fields': self.fields['extension'],
                'func': lambda arr: {source[0]: source[1] for source in arr}
            },
            {
                'fields': self.fields['cert'],
                'func': lambda cert_str: True if cert_str else False
            },
            # {
            #     'fields': self.fields['content'],
            #     'func': lambda content_str: content_str.replace("\"", "")
            # }
        ]

        if self.encode:
            self.transformations.append({
                'fields': self.fields['content'],
                'func': base64.b64encode,
            })

    @staticmethod
    def transform_field(obj, path, func):
        """
        Apply a transformation function `func` to a value
        under the specified `path` in the `obj` dictionary.
        """
        for key in path[: -1]:
            if not (key in obj and obj[key]):
                return
            obj = obj[key]
        if path[-1] in obj and obj[path[-1]]:
            obj[path[-1]] = func(obj[path[-1]])

    @classmethod
    def convert_to_strings(cls, obj):
        """
        Recursively convert all list/dict elements of type `bytes` into strings.
        """
        if isinstance(obj, dict):
            return {cls.convert_to_strings(key): cls.convert_to_strings(value)
                    for key, value in obj.items()}
        elif isinstance(obj, list) or isinstance(obj, tuple):
            return [cls.convert_to_strings(element) for element in obj]
        elif isinstance(obj, bytes):
            return str(obj)[2:-1]
        return obj

    def worker(self):
        while True:
            frame = self.queue.get()
            self.dump(frame)
            self.queue.task_done()

    def dump(self, frame):
        """
        Transform and dump (write / send) a data frame.
        """
        for tfm in self.transformations:
            for field in tfm['fields']:
                self.transform_field(frame, field, tfm['func'])
        frame = self.convert_to_strings(frame)

        if self.outfile:
            self.lock.acquire()
            self.outfile.write(json.dumps(frame) + "\n")
            self.lock.release()
        else:
            requests.post(self.url, json=frame, auth=(self.auth or None))

    @staticmethod
    def load(loader):
        """
        Extra options to be specified in `~/.mitmproxy/config.yaml`.
        """
        loader.add_option('dump_destination', str, DEFAULT_DUMP_DESTINATION,
                        'Output destination: path to a file or URL.')
        loader.add_option('dump_encodecontent', bool, False,
                        'Encode content as base64.')
        loader.add_option('index_name', str, DEFAULT_INDEX_NAME,
                        'Elastic search index name, where the data gets stored.')
        loader.add_option('dump_username', str, '',
                        'Basic auth username for URL destinations.')
        loader.add_option('dump_password', str, '',
                        'Basic auth password for URL destinations.')
            

    def test_connection(self, base_url):
        try:
            response = requests.get(base_url)
            if response.status_code == 200:
                ctx.log.debug(
                    "dump: Testing Route: {}, was succesfull".format(base_url))

        except Exception as e:
            ctx.log.error(traceback.format_exc())
            raise e
    
    def get_initial_mapping(self):
        return {
            "mappings": {
                "properties": {
                "request.timestamp_start": {
                    "type": "date",
                    "format": "strict_date_optional_time||epoch_millis"
                },
                "request.timestamp_tls_setup": {
                    "type": "date",
                    "format": "strict_date_optional_time||epoch_millis"
                },
                "request.content": {
                    "type": "text",
                    "fielddata": "true"
                },
                "respnse.content": {
                    "type": "text",
                    "fielddata": "true"
                },
                "request.headers.Content-Length": {
                    "type": "long"
                },
                "respnse.headers.Content-Length": {
                    "type": "long"
                }
                }
            }
        }

    def create_index(self, base_url, index_name):
        try:
            url = "{}{}".format(base_url, index_name)
            response = requests.put(url, json=self.get_initial_mapping())
            if response.status_code == 200:
                ctx.log.debug(
                    "dump: Creating index: {}, was succesfull".format(index_name))
            else:
                ctx.log.error(
                    "dump: Creating index: {}, has failed".format(index_name))
                raise

        except Exception as e:
            ctx.log.debug(traceback.format_exc())
            raise e

    def index_exists(self, base_url, index_name):
        try:
            url = "{}{}".format(base_url, index_name)
            response = requests.get(url)
            if response.status_code == 200:
                ctx.log.debug(
                    "dump: Index {} exists.".format(url))
                return True
            else:
                ctx.log.error(
                    "jsondump: Index {} does not exist, trying to create it...".format(url))
                return False

        except Exception as e:
            ctx.log.error(traceback.format_exc())
            return False

    def configure(self, _):
        """
        Determine the destination type and path, initialize the output
        transformation rules.
        """
        destination = ctx.options.dump_destination
        index = ctx.options.index_name
        self.encode = ctx.options.dump_encodecontent

        ctx.log.debug(
            "dump: using variables: destination: {}, index: {}, encode: {}".format(destination, index, self.encode))

        if destination.startswith('http'):
            self.test_connection(destination)
            if index:
                if not self.index_exists(destination, index):
                    self.create_index(destination, index)
            else:
                ctx.log.error(
                    "jsondump: index_name was not provided. Cannot store data on an empty index.")
            try:
                self.outfile = None
                self.url = "{}{}{}".format(
                    destination, index, "/_doc")
                ctx.log.debug('dump: Sending all data frames to %s' % self.url)
                if ctx.options.dump_username and ctx.options.dump_password:
                    self.auth = (ctx.options.dump_username,
                                 ctx.options.dump_password)
                    ctx.log.debug('dump: HTTP Basic auth enabled.')

            except Exception as e:
                ctx.log.error(traceback.format_exc())
                ctx.log.error(
                    "jsondump: Exiting the script!")
                raise

        else:
            try:
                self.outfile = open(ctx.options.dump_destination, 'a')
            except Exception as e:
                ctx.log.error(traceback.format_exc())
                ctx.log.error(
                    "jsondump: Exiting the script!")
                raise
            self.url = None
            self.lock = Lock()
            ctx.log.info('jsondump: Writing all data frames to %s' %
                         ctx.options.dump_destination)

        self._init_transformations()

        for i in range(FILE_WORKERS if self.outfile else HTTP_WORKERS):
            t = Thread(target=self.worker)
            t.daemon = True
            t.start()

    def response(self, flow):
        """
        Dump request/response pairs.
        """
        self.queue.put(flow.get_state())

    def error(self, flow):
        """
        Dump errors.
        """
        self.queue.put(flow.get_state())

    def websocket_end(self, flow):
        """
        Dump websocket messages once the connection ends.
        Alternatively, you can replace `websocket_end` with
        `websocket_message` if you want the messages to be
        dumped one at a time with full metadata. Warning:
        this takes up _a lot_ of space.
        """
        self.queue.put(flow.get_state())

    def websocket_error(self, flow):
        """
        Dump websocket errors.
        """
        self.queue.put(flow.get_state())


addons = [JSONDumper()]  # pylint: disable=invalid-name
