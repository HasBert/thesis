#!/bin/bash


#############################################
# Created by mobilsicher.de                 # 
#############################################

# run as 'root' and remount filesystem as r/w
adb root
adb remount

# check for mitmproxy CA cert
CERT=~/.mitmproxy/mitmproxy-ca-cert.pem
if [ -z ${CERT} ]; then
	echo "No mitmproxy CA certificate found."
	echo "Start and quit 'mitmproxy', then try again..."
	exit 1
fi

# compute fingerprint of certificate (used as filename)
HASH=$(openssl x509 -noout -subject_hash_old -in ${CERT})

# copy the certificate to the trusted cert store
adb push ${CERT} /system/etc/security/cacerts/${HASH}.0

# unroot the device
adb unroot
exit 0
