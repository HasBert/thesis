#!/bin/bash

# rewrwote orginal entrypoint
set -e
MITMPROXY_PATH="/home/mitmproxy/.mitmproxy"

exec_mitmproxy () {
        mkdir -p "$MITMPROXY_PATH"
        chown -R mitmproxy:mitmproxy "$MITMPROXY_PATH"
        echo "bash: start command is <$@>"
        su-exec mitmproxy "$@"
}

if [ "$1" = "" ]; then
        echo "bash: You need to specify at least one mitmproxy input!"

elif [[ "$1" = "mitmdump" || "$1" = "mitmproxy" || "$1" = "mitmweb" ]]; then
        echo "bash: Starting <$1> without waiting for a service."
        exec_mitmproxy "$@"

elif [[ "$2" = "mitmdump" || "$2" = "mitmproxy" || "$2" = "mitmweb" ]]; then
        echo "bash: Wait for service <$1> and start <$2> afterwards."

        python3 /wait-for-elastic.py $1
        if [ "$?" -eq "0" ]; then
                echo "bash: source <$1> is available"
                COMMANDS=("$@")
                SLICED_COMMANDS=("${COMMANDS[@]:1}")
                echo "bash: sliced command array to <${SLICED_COMMANDS[@]}>"
                exec_mitmproxy $SLICED_COMMANDS
        else
                echo "bash: source <$1> is NOT available"
        fi

else 
        echo "bash: executing normally"
        exec "$@"
fi
