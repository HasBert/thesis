import sys
import requests
import time
import os

args = sys.argv[1:]
route = "http://{}".format(args[0])

retries = 20
timeout = 5
retrying = True
while retrying:
    try:
        response = requests.get(route)
        if (response.status_code == 200):
            retrying = False
            print("python: Route {} is now available!".format(route))
            # execute the other commands
            exit(0)
    except Exception as exception:
        if not retries:
            seconds = retries * timeout
            print("Route {} is not available! after {} seconds. Exiting!".format(route, seconds))
            exit(1)
        
        print("python: Route {} is not available! Retrying for {} more times.".format(
            route, retries))
        retries -= 1
        time.sleep(timeout)

