import csv
import json

def json_from_file(filename):
    try:
        with open(filename) as json_file:
            data = json.load(json_file)
            return data
    except Exception as error:
        Log.error("File {} not available".format(filename))

def to_csv(data):
    with open('topcharts.csv', mode='w') as topcharts:
        writer = csv.writer(topcharts, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
        writer.writerow(['title', 'package_name', 'version', 'rating', 'category', 'downloads'])
        for item in data: 
            writer.writerow([item['title'], item['package_name'], item['version'], item['rating'], item['category'], item['downloads'].replace(",", ".")])

keys = [
    "title",
    "package_name",
    "version",
    "rating",
    "category",
    "downloads"
]

data = json_from_file("topcharts.json")
dic = [{key: item[key] for key in keys} for item in data["app_list"]]
print(dic)
to_csv(dic)